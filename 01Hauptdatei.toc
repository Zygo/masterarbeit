\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\contentsline {chapter}{\nonumberline Abbildungs- und Tabellenverzeichnis}{8}{chapter*.2}% 
\contentsline {chapter}{\nonumberline Abk\IeC {\"u}rzungsverzeichnis}{12}{chapter*.5}% 
\contentsline {chapter}{\numberline {0}\IeC {\"U}bersicht}{14}{chapter.0}% 
\contentsline {chapter}{\numberline {1}Umweltsimulation: Klimakammer}{15}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Typen und Anwendungsm\IeC {\"o}glichkeiten}{15}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Temperatur und Temperaturmessung}{16}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Luftfeuchte und Luftfeuchtemessung}{18}{section.1.3}% 
\contentsline {section}{\numberline {1.4}Der PID Regler}{23}{section.1.4}% 
\contentsline {section}{\numberline {1.5}Die Klimakammer bei WAVELABS: Ausgangssituation}{27}{section.1.5}% 
\contentsline {chapter}{\numberline {2}Geforderte Ziele des Gesamtsystem}{30}{chapter.2}% 
\contentsline {chapter}{\numberline {3}Konzeptionierung und Planung}{32}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Die Dampferzeugungskomponenten}{32}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Mess- und Steuerelektronik}{34}{section.3.2}% 
\contentsline {subsubsection}{\nonumberline Steuereinheit mit Mikrocontroller}{34}{section*.11}% 
\contentsline {subsubsection}{\nonumberline Sensor f\IeC {\"u}r Temperatur und Luftfeuchte}{38}{section*.14}% 
\contentsline {subsubsection}{\nonumberline Detektorschaltung f\IeC {\"u}r den Zylinderf\IeC {\"u}llstand}{40}{section*.16}% 
\contentsline {subsubsection}{\nonumberline Schaltung f\IeC {\"u}r die Ansteuerung des Magnetventils}{41}{section*.18}% 
\contentsline {subsubsection}{\nonumberline Schaltung f\IeC {\"u}r die Ansteuerung des Dampfzylinders}{41}{section*.20}% 
\contentsline {subsection}{\numberline {3.2.1}Simulation}{42}{subsection.3.2.1}% 
\contentsline {subsubsection}{\nonumberline Detektorschaltung f\IeC {\"u}r den Zylinderf\IeC {\"u}llstand}{42}{section*.21}% 
\contentsline {subsubsection}{\nonumberline Schaltung f\IeC {\"u}r die Ansteuerung eines Relais}{46}{section*.26}% 
\contentsline {subsection}{\numberline {3.2.2}Spezifikation der anderen Elektronik}{50}{subsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.2.3}Platinenlayout und der Schaltkasten}{53}{subsection.3.2.3}% 
\contentsline {section}{\numberline {3.3}Fehlersuche und Inbetriebnahme}{55}{section.3.3}% 
\contentsline {section}{\numberline {3.4}Arbeitsmittel: Software}{64}{section.3.4}% 
\contentsline {section}{\numberline {3.5}Skizze des Gesamtprojektes}{65}{section.3.5}% 
\contentsline {section}{\numberline {3.6}Risikobeurteilung}{67}{section.3.6}% 
\contentsline {section}{\numberline {3.7}Kostenbetrachtung}{68}{section.3.7}% 
\contentsline {chapter}{\numberline {4}Systembeschreibung}{69}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Das Luftfeuchtesystem}{69}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}Mechanik und Elektrik}{69}{subsection.4.1.1}% 
\contentsline {subsection}{\numberline {4.1.2}Mikrocontroller Programmierung}{73}{subsection.4.1.2}% 
\contentsline {section}{\numberline {4.2}Das Temperatursystem}{78}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Die Regler}{78}{section.4.3}% 
\contentsline {subsection}{\numberline {4.3.1}Temperaturregler}{78}{subsection.4.3.1}% 
\contentsline {subsection}{\numberline {4.3.2}Feuchteregler}{82}{subsection.4.3.2}% 
\contentsline {subsection}{\numberline {4.3.3}Zusammenf\IeC {\"u}hrung der Regler und deren Programmcode}{85}{subsection.4.3.3}% 
\contentsline {section}{\numberline {4.4}Das Programm und die Benutzeroberfl\IeC {\"a}che}{89}{section.4.4}% 
\contentsline {subsection}{\numberline {4.4.1}Tab: Regelwert}{94}{subsection.4.4.1}% 
\contentsline {subsection}{\numberline {4.4.2}Tab: Regelkurve}{96}{subsection.4.4.2}% 
\contentsline {subsection}{\numberline {4.4.3}Tab: Konfiguration}{101}{subsection.4.4.3}% 
\contentsline {subsection}{\numberline {4.4.4}Tab: Wartung}{104}{subsection.4.4.4}% 
\contentsline {chapter}{\numberline {5}Abschlie\IeC {\ss }ende Messungen}{105}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Systemtest}{105}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Ergebnisse}{110}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Arbeitsbereich der leeren Klimakammer}{110}{subsection.5.2.1}% 
\contentsline {subsection}{\numberline {5.2.2}Arbeitsbereich mit laufendem SINUS-220}{110}{subsection.5.2.2}% 
\contentsline {section}{\numberline {5.3}Vergleichsmessungen}{111}{section.5.3}% 
\contentsline {section}{\numberline {5.4}Auswertung}{113}{section.5.4}% 
\contentsline {chapter}{\numberline {6}Res\IeC {\"u}mee und Ausblick}{116}{chapter.6}% 
\contentsline {part}{\nonumberline Anhang}{128}{part*.80}% 
